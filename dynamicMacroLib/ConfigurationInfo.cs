﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dynamicMacroLib
{
    public class ConfigurationInfo
    {
        public ConfigurationInfo()
        {
            PageIds = new HashSet<string>();
            EMode = ExcelMode.OPEN;
            MainShtName = string.Empty;
            PrimeCol = 1;
            CheckCol = 1;
            AdditionalInfoCol = 1;
            ClientNumberCell = "A1";
        }

        public HashSet<string> PageIds { get; set; }
        public ExcelMode EMode { get; set; }
        public string MainShtName { get; set; }
        public int PrimeCol { get; set; }
        public int CheckCol { get; set; }
        public int AdditionalInfoCol { get; set; }
        public string ClientNumberCell { get; set; }
    }
}
