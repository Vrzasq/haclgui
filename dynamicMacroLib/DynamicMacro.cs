﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCOMMLib;
using Excel = Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using System.IO;

namespace dynamicMacroLib
{
    public enum ExcelMode
    {
        NONE, OPEN, CREATE
    }

    public abstract class DynamicMacro
    {
        private bool runMacro;

        protected PCOMMSession pcommSession;
        protected PCOMMFieldList pcommFieldList;
        protected PCOMMOIA pcommOIA;
        protected PCOMMPSpace pcommPS;
        protected ConfigurationInfo basicConfig;

        protected Excel.Application excellApp;
        protected Excel.Workbook mainWrk;
        protected Excel.Worksheet mainSht;

        protected object additionlData;

        /// <summary>
        /// Happens after Run method is comlpleted
        /// </summary>
        public event EventHandler<MacroEventArgs> Complete;

        /// <summary>
        /// Whenever error occured
        /// </summary>
        public event EventHandler<MacroEventArgs> Error;

        public virtual void INIT(string connectionName)
        {
            runMacro = true;
            PCommInit(connectionName);
            basicConfig = SetConfiguration<ConfigurationInfo>();
            CheckPageID();
            if (runMacro)
                ExcelInit();
            if (runMacro)
                AdditionInitIstruction();

        }

        protected virtual void AdditionInitIstruction() { }

        public void Run()
        {
            if (runMacro)
            {
                MAIN();
                OnComplete(new MacroEventArgs
                {
                    SessionName = pcommSession.Name,
                    AdditionalInfo = "Done",
                    AdditionalData = additionlData,
                    UnSub = true
                });
            }
        }

        protected T SetConfiguration<T>() where T : ConfigurationInfo, new()
        {
            T conf;
            string configurationFilePath = "macros_dll\\config_files\\" + ConfigurationFileName();
            if (File.Exists(configurationFilePath))
            {
                conf = JsonConvert.DeserializeObject<T>(File.ReadAllText(configurationFilePath));
                return conf;
            }

            conf = SetDefaultConfiguration<T>();

            OnError(new MacroEventArgs
            {
                SessionName = pcommSession.Name,
                AdditionalInfo = "You did no provided config file: " + ConfigurationFileName(),
                AdditionalData = conf,
                UnSub = false
            });

            return conf;
        }


        /// <summary>
        /// Override if You don't provide json configuration file
        /// </summary>
        /// <returns></returns>
        protected virtual T SetDefaultConfiguration<T>() where T : ConfigurationInfo, new()
        {
            return new T();
        }

        /// <summary>
        /// configuration file name
        /// </summary>
        /// <returns></returns>
        protected abstract string ConfigurationFileName();


        /// <summary>
        /// Implent body if You use ExcelMode.CREATE
        /// </summary>
        protected abstract void ExcelInitCreate();

        /// <summary>
        /// Main macro body, this is where all the magic happen
        /// </summary>
        protected abstract void MAIN();

        /// <summary>
        /// Complete handler e.AdditionalData is null by default
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnComplete(MacroEventArgs e)
        {
            var hadler = Complete;
            if (hadler != null)
                hadler(this, e);
        }

        protected virtual void OnError(MacroEventArgs e)
        {
            var handler = Error;
            if (handler != null)
                handler(this, e);
        }

        protected string GetPid(string s)
        {
            if (s.Length != 10 && s.StartsWith("8"))
                return "0" + s;
            if (s.Length != 10 && s.StartsWith("6"))
                return "00" + s;
            return s;
        }

        protected string GetExcelCellValue(Excel.Range range)
        {
            if (range.Value2 != null)
            {
                if (range.Value2.GetType() != typeof(double))
                    return ((string)range.Value2).Trim();
                else
                    return range.Value2.ToString();
            }

            return null;
        }

        private void ExcelInit()
        {
            switch (basicConfig.EMode)
            {
                case ExcelMode.NONE:
                    break;
                case ExcelMode.OPEN:
                    ExcelInitOpen();
                    break;
                case ExcelMode.CREATE:
                    ExcelInitCreate();
                    break;
                default:
                    break;
            }
        }

        private void ExcelInitOpen()
        {
            excellApp = new Excel.Application();
            dynamic file = excellApp.GetOpenFilename();

            string shtName = basicConfig.MainShtName;

            try
            {
                string test = (string)file;
            }
            catch
            {
                file = null;
            }

            if (!string.IsNullOrEmpty(file))
            {
                var tempWrks = excellApp.Workbooks;
                mainWrk = tempWrks.Open(file);
                if (!string.IsNullOrEmpty(shtName))
                    mainSht = mainWrk.Worksheets[shtName];
                else
                    mainSht = mainWrk.Worksheets[1];

                excellApp.Visible = true;
            }
            else
            {
                runMacro = false;
                excellApp.Quit();
                OnError(new MacroEventArgs
                {
                    SessionName = pcommSession.Name,
                    AdditionalInfo = "Excel file missing, macro will exit",
                    AdditionalData = additionlData,
                    UnSub = true
                });
            }
        }

        private void PCommInit(string connectionName)
        {
            pcommSession = new PCOMMSession(connectionName);
            pcommPS = pcommSession.PSpace;
            pcommOIA = pcommSession.OIA;
            pcommFieldList = pcommPS.FieldList;
        }

        private void CheckPageID()
        {
            pcommFieldList.Refresh();
            string id = pcommFieldList.GetFieldData(02, 072);
            HashSet<string> pageID = basicConfig.PageIds;
            if (!pageID.Contains(id) && pageID.Count > 0)
            {
                runMacro = false;
                OnError(new MacroEventArgs
                {
                    SessionName = pcommSession.Name,
                    AdditionalInfo = "You can't run this macro on [page ID]: " + id,
                    AdditionalData = additionlData,
                    UnSub = true
                });
            }
        }
    }
}