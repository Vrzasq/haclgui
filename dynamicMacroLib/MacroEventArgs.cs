﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dynamicMacroLib
{
    public class MacroEventArgs : EventArgs
    {
        public string SessionName { get; set; }
        public string AdditionalInfo { get; set; }
        public object AdditionalData { get; set; }
        public bool UnSub { get; set; }
    }
}
