﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dynamicMacroLib;

namespace searchXOcosPL
{
    public class Macro : DynamicMacro
    {
        private string configFileName = "searchXOcosPL.json";

        protected override string ConfigurationFileName()
        {
            return configFileName;
        }

        protected override void ExcelInitCreate()
        {
            return;
        }

        protected override void MAIN()
        {
            int i = 2;
            string mainField = GetExcelCellValue(mainSht.Cells[i, basicConfig.PrimeCol]);

            while (!string.IsNullOrEmpty(mainField))
            {
                string checkField = GetExcelCellValue(mainSht.Cells[i, basicConfig.CheckCol]);

                if (string.IsNullOrEmpty(checkField))
                {
                    string pid = GetPid(mainField);
                    pcommPS.SetText(pid, 04, 034);
                    pcommPS.SendKeys(PCOMMLib.PCOMMButtons.ENTER);
                    pcommOIA.WaitForPCOMM();

                    pcommFieldList.Refresh();
                    string cosPID = pcommFieldList.GetFieldData(11, 05);

                    if (cosPID == pid)
                    {
                        pcommPS.SetText("5", 11, 002);
                        pcommPS.SendKeys(PCOMMLib.PCOMMButtons.ENTER);
                        pcommOIA.WaitForPCOMM();

                        for (int cosRow = 0; cosRow < 3; cosRow++)
                        {
                            pcommPS.SendKeys(PCOMMLib.PCOMMButtons.PAGE_UP);
                            pcommOIA.WaitForPCOMM();
                        }

                        pcommFieldList.Refresh();
                        string code = pcommFieldList.GetFieldData(08, 045);
                        mainSht.Cells[i, basicConfig.AdditionalInfoCol].Value2 = code;


                        pcommPS.SendKeys(PCOMMLib.PCOMMButtons.F12);
                        pcommOIA.WaitForPCOMM();

                    }
                    else
                    {
                        mainSht.Cells[i, basicConfig.AdditionalInfoCol].Value2 = "pid not found";
                    }

                    pcommPS.ClearField(04, 034);
                    mainSht.Cells[i, basicConfig.CheckCol].Value2 = "check";

                }

                i++;
                mainField = GetExcelCellValue(mainSht.Cells[i, basicConfig.PrimeCol]);
            }
        }
    }
}
