﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dynamicMacroLib;

namespace getHpOrderNR
{
    public class Macro : DynamicMacro
    {
        private string configFileName = "getHpOrderNR.json";

        protected override string ConfigurationFileName()
        {
            return configFileName;
        }

        protected override void ExcelInitCreate()
        {
            return;
        }

        protected override void MAIN()
        {
            var clientNumber = GetExcelCellValue(mainSht.Range[basicConfig.ClientNumberCell]);

            ClearBasicField();
            pcommPS.SetText(clientNumber, 03, 033);

            int i = 2;
            string primeField = GetExcelCellValue(mainSht.Cells[i, basicConfig.PrimeCol]);
            while (!string.IsNullOrEmpty(primeField))
            {
                string pid = GetPid(primeField);
                pcommPS.ClearField(03, 054);
                pcommPS.SetText(pid, 03, 054);
                pcommPS.SendKeys(PCOMMLib.PCOMMButtons.ENTER);
                pcommOIA.WaitForPCOMM();

                pcommPS.SetText("10", 10, 002);
                pcommPS.SendKeys(PCOMMLib.PCOMMButtons.ENTER);
                pcommOIA.WaitForPCOMM();

                pcommFieldList.Refresh();
                string hpOrderNR = pcommFieldList.GetFieldData(10, 005);

                mainSht.Cells[i, basicConfig.AdditionalInfoCol].Value2 = hpOrderNR;

                pcommPS.SendKeys(PCOMMLib.PCOMMButtons.F12);
                pcommOIA.WaitForPCOMM();

                i++;
                primeField = GetExcelCellValue(mainSht.Cells[i, basicConfig.PrimeCol]);
            }

        }

        private void ClearBasicField()
        {
            pcommPS.ClearField(03, 009);
            pcommPS.ClearField(03, 020);
            pcommPS.ClearField(03, 033);
            pcommPS.ClearField(03, 054);
            pcommPS.ClearField(04, 008);
            pcommPS.ClearField(04, 048);
            pcommPS.ClearField(05, 036);
            pcommPS.ClearField(05, 063);
        }
    }
}
