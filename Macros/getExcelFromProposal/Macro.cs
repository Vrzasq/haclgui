﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dynamicMacroLib;
using PCOMMLib;

namespace getExcelFromProposal
{
    public class Macro : DynamicMacro
    {
        List<Proposal> proposals = new List<Proposal>();
        private string configFilename = "getExcelFromProposal.json"; //TODO Add File name here

        protected override string ConfigurationFileName()
        {
            return configFilename;
        }

        protected override void ExcelInitCreate() { return; }


        protected override void MAIN()
        {
            SearchProposals();
            MoveToRaportgeneration();
            GetReportFileName();
        }

        private void GetReportFileName()
        {
            foreach (Proposal proposal in proposals)
            {
                if (proposal.ReportFileName == null)
                {
                    pcommPS.ClearField(08, 041);
                    //pcommPS.SetText(proposal.PartyID, 08, 041);
                    pcommPS.SetText(proposal.RunID, 09, 041);
                    pcommPS.SendKeys(PCOMMButtons.F10);
                    pcommOIA.WaitForPCOMM();
                    pcommFieldList.Refresh();

                    pcommPS.SetText("HP SELFBIL", 03, 036);
                    pcommPS.SendKeys(PCOMMButtons.ENTER);
                    pcommOIA.WaitForPCOMM();
                    pcommPS.Wait(500);

                    if (proposal.IsCHF)
                        pcommPS.SetText("L", 18, 025);
                    else
                        pcommPS.SetText("E", 18, 025);

                    pcommPS.ClearField(18, 049);
                    pcommPS.ClearField(19, 049);
                    pcommPS.SetText(proposal.ClientID, 18, 049);
                    pcommPS.SetText(proposal.PartyID, 19, 049);
                    pcommPS.SendKeys(PCOMMButtons.F10);
                    pcommOIA.WaitForPCOMM();
                    pcommFieldList.Refresh();

                    string reportFileName = pcommFieldList.GetFieldData(10, 019);
                    mainSht.Cells[proposal.ExcelRow, 7].Value = reportFileName.Substring(13);

                    pcommPS.SendKeys(PCOMMButtons.F12);
                    pcommOIA.WaitForPCOMM();
                    pcommPS.Wait(500);
                    pcommPS.SetText("60", 20, 006);
                    pcommPS.SendKeys(PCOMMButtons.ENTER);
                    pcommOIA.WaitForPCOMM();
                    pcommPS.Wait(500);
                }
            }
        }

        private void MoveToRaportgeneration()
        {
            pcommPS.SendKeys(PCOMMButtons.F12);
            pcommOIA.WaitForPCOMM();
            pcommPS.Wait(500);
            pcommPS.SendKeys(PCOMMButtons.F12);
            pcommOIA.WaitForPCOMM();
            pcommPS.Wait(500);
            pcommPS.SetText("70", 20, 006);
            pcommPS.SendKeys(PCOMMButtons.ENTER);
            pcommOIA.WaitForPCOMM();
            pcommPS.Wait(500);
            pcommPS.SetText("80", 20, 006);
            pcommPS.SendKeys(PCOMMButtons.ENTER);
            pcommOIA.WaitForPCOMM();
            pcommPS.Wait(500);
            pcommPS.SetText("60", 20, 006);
            pcommPS.SendKeys(PCOMMButtons.ENTER);
            pcommOIA.WaitForPCOMM();
            pcommPS.Wait(500);
            pcommFieldList.Refresh();
        }

        private void SearchProposals()
        {
            int row = 2;


            string proposalID = GetExcelCellValue(mainSht.Cells[row, basicConfig.PrimeCol]);
            string partyID = pcommFieldList.GetFieldData(05, 039);

            do
            {
                Proposal proposal;
                string isLoaded = GetExcelCellValue(mainSht.Cells[row, 6]);

                if (isLoaded != null)
                    proposal = GetFromExcel(row);
                else
                {
                    proposal = new Proposal();
                    proposal.ProposalID = proposalID;
                    proposal.PartyID = partyID;
                    proposal.ClientID = GetExcelCellValue(mainSht.Cells[row, basicConfig.AdditionalInfoCol]);
                    proposal.ExcelRow = row;

                    for (int i = 11; i <= 20; i++)
                    {
                        var docID = pcommFieldList.GetFieldData(i, 027);
                        if (docID == proposal.ProposalID)
                        {
                            proposal.RunID = pcommFieldList.GetFieldData(i, 005);
                            string cur = pcommFieldList.GetFieldData(i, 072);
                            if (cur != "EUR")
                                proposal.IsCHF = true;
                            PutToExcel(proposal, row);
                            break;
                        }

                        if (i == 20)
                        {
                            pcommPS.SendKeys(PCOMMLib.PCOMMButtons.PAGE_DOWN);
                            pcommOIA.WaitForPCOMM();
                            pcommFieldList.Refresh();
                            i = 10;
                        }
                    }
                }


                proposals.Add(proposal);
                row++;
                proposalID = GetExcelCellValue(mainSht.Cells[row, basicConfig.PrimeCol]);
            } while (proposalID != null);

        }

        private Proposal GetFromExcel(int row)
        {
            var proposal = new Proposal();
            proposal.ExcelRow = row;
            proposal.ProposalID = GetExcelCellValue(mainSht.Cells[row, 1]);
            proposal.ClientID = GetExcelCellValue(mainSht.Cells[row, 2]);
            proposal.RunID = GetExcelCellValue(mainSht.Cells[row, 3]);
            proposal.PartyID = GetExcelCellValue(mainSht.Cells[row, 4]);
            var isChf = GetExcelCellValue(mainSht.Cells[row, 5]);
            proposal.IsCHF = isChf == "yes" ? true : false;
            proposal.ReportFileName = GetExcelCellValue(mainSht.Cells[row, 7]);

            return proposal;
        }

        private void PutToExcel(Proposal proposal, int row)
        {
            mainSht.Cells[row, 3].Value = proposal.RunID;
            mainSht.Cells[row, 4].Value = proposal.PartyID;
            if (proposal.IsCHF)
                mainSht.Cells[row, 5].Value = "yes";
            else
                mainSht.Cells[row, 5].Value = "no";
            mainSht.Cells[row, 6].Value = "yes";
        }

        protected override T SetDefaultConfiguration<T>()
        {
            return base.SetDefaultConfiguration<T>();
        }
    }
}
