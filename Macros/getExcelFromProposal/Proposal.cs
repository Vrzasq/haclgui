﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getExcelFromProposal
{
    public class Proposal
    {
        public string ProposalID { get; set; }
        public string RunID { get; set; }
        public string ClientID { get; set; }
        public bool IsCHF { get; set; }
        public string PartyID { get; set; }
        public string ReportFileName { get; set; }
        public int ExcelRow { get; set; }
    }
}