﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fillAddonTempFCDN
{
    class Addon
    {
        public Addon()
        {
            Pids = new List<string>();
        }

        public string CustomerID { get; set; }
        public int BoxAmount { get; set; }
        public double Weight { get; set; }
        public DateTime ShipmentDate { get; set; }
        public string RecipentName { get; set; }
        public string RecipentCity { get; set; }
        public string RecipentAddress { get; set; }
        public string RecipentCoCode { get; set; }
        public string RecipentCoName { get; set; }
        public string RecipentPostal { get; set; }
        public string ShipperCompanyName { get; set; }
        public string ShipperAddress { get; set; }
        public string ShipperCity { get; set; }
        public string ShipperPostal { get; set; }
        public string ShipperCoCode { get; set; }
        public string ShipperCoName { get; set; }
        public string PackID { get; set; }
        public string FlowIdentifier { get; set; }
        public string HpCompanyName { get; set; }
        public string BU { get; set; }
        public string FCDN { get; set; }
        public string CarrierAccountNumber { get; set; }
        public List<string> Pids { get; set; }
        public string ConsigneeID { get; set; }
        public string ServiceLevel { get; set; } = string.Empty;
    }
}