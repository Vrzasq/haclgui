﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dynamicMacroLib;
using DbAccessLayer;
using System.Globalization;

namespace fillAddonTempFCDN
{
    public class Macro : DynamicMacro
    {
        private string configFileName = "fillAddonTempFCDN.json";
        private Headers headers;
        private AddonConfigurationInfo additionalConfig;
        private string shortCompanyName;

        protected override void ExcelInitCreate() { return; }

        protected override string ConfigurationFileName()
        {
            return configFileName;
        }

        protected override void AdditionInitIstruction()
        {
            additionalConfig = SetConfiguration<AddonConfigurationInfo>();
            headers = additionalConfig.Headers;
            headers = SetConfiguration<AddonConfigurationInfo>().Headers;
            shortCompanyName = GetExcelCellValue(mainSht.Range[basicConfig.ClientNumberCell]);
        }

        protected override void MAIN()
        {
            VDbConnection data = new VDbConnection();

            int row = 25;
            string fcdn = GetExcelCellValue(mainSht.Cells[row, basicConfig.PrimeCol]);

            do
            {
                string shipDateValue = GetExcelCellValue(mainSht.Cells[row, basicConfig.CheckCol]);
                if (shipDateValue == null)
                {
                    pcommPS.ClearField(03, 037);
                    pcommPS.SetText(fcdn, 03, 037);
                    pcommPS.SendKeys(PCOMMLib.PCOMMButtons.ENTER);
                    pcommOIA.WaitForPCOMM();

                    pcommPS.Wait(100);
                    pcommPS.SetText("5", 10, 002);
                    pcommPS.SendKeys(PCOMMLib.PCOMMButtons.ENTER);
                    pcommOIA.WaitForPCOMM();

                    Addon addon = GetAddonData(data, row);

                    pcommPS.SendKeys(PCOMMLib.PCOMMButtons.F12);
                    pcommOIA.WaitForPCOMM();

                    SetPIDsAndWeight(addon);
                    FillAddonTemplate(row, addon, data);
                }

                row++;
                fcdn = GetExcelCellValue(mainSht.Cells[row, basicConfig.PrimeCol]);
            } while (fcdn != null);

            data.Dispose();
        }

        private void SetPIDsAndWeight(Addon addon)
        {
            List<string> pids = new List<string>();
            double weight = 0.00;
            int box = 0;
            string bottom = "";

            do
            {
                pcommPS.Wait(500);
                pcommFieldList.Refresh();

                bottom = pcommFieldList.GetFieldData(20, 079);

                for (int i = 10; i <= 19; i++)
                {
                    string nValue = pcommFieldList.GetFieldData(i, 078);
                    if (nValue != "N")
                        continue;
                    else
                    {
                        string pid = pcommFieldList.GetFieldData(i, 049);
                        pids.Add(pid);

                        pcommPS.SetText("5", i, 002);
                        pcommPS.SendKeys(PCOMMLib.PCOMMButtons.ENTER);
                        pcommOIA.WaitForPCOMM();

                        weight += GetWeight();
                        box += GetBoxes();

                        pcommPS.SendKeys(PCOMMLib.PCOMMButtons.F12);
                        pcommOIA.WaitForPCOMM();
                        pcommPS.Wait(100);
                        pcommFieldList.Refresh();
                    }
                }

                if (bottom != "Bottom")
                {
                    pcommPS.SendKeys(PCOMMLib.PCOMMButtons.PAGE_DOWN);
                    pcommOIA.WaitForPCOMM();
                    pcommPS.Wait(100);
                    pcommFieldList.Refresh();
                }

            } while (bottom != "Bottom");


            addon.Pids = pids;
            addon.Weight = weight;
            addon.BoxAmount = box;
        }

        private int GetBoxes()
        {
            int boxes = 0;
            string boxString = pcommFieldList.GetFieldData(16, 023);
            boxes = int.Parse(boxString);
            return boxes;
        }

        private double GetWeight()
        {
            double weight = 0.0;

            pcommFieldList.Refresh();
            string weightString = pcommFieldList.GetFieldData(16, 064);
            weight = double.Parse(weightString);

            return weight;
        }

        private Addon GetAddonData(VDbConnection data, int row)
        {
            Addon addon = new Addon();

            pcommFieldList.Refresh();

            addon.ConsigneeID = pcommFieldList.GetFieldData(06, 017);
            addon.PackID = pcommFieldList.GetFieldData(04, 020);
            addon.CustomerID = pcommFieldList.GetFieldData(03, 020);
            addon.ShipmentDate = DateTime.ParseExact((pcommFieldList.GetFieldData(06, 073)), "yyyy-MM-dd", CultureInfo.InvariantCulture);
            addon.RecipentName = pcommFieldList.GetFieldData(07, 017);
            addon.RecipentAddress = pcommFieldList.GetFieldData(09, 017) + " " + pcommFieldList.GetFieldData(10, 017);
            addon.RecipentCity = pcommFieldList.GetFieldData(11, 017);
            addon.RecipentPostal = pcommFieldList.GetFieldData(12, 017);
            addon.RecipentCoCode = pcommFieldList.GetFieldData(13, 017);
            addon.RecipentCoName = data.countries.Where(x => x.co_code == addon.RecipentCoCode).FirstOrDefault().co_name;

            pcommPS.SendKeys(PCOMMLib.PCOMMButtons.PAGE_DOWN);
            pcommOIA.WaitForPCOMM();

            pcommFieldList.Refresh();

            addon.ShipperCompanyName = pcommFieldList.GetFieldData(07, 017);
            addon.ShipperAddress = pcommFieldList.GetFieldData(09, 017);
            addon.ShipperCity = pcommFieldList.GetFieldData(11, 017);
            addon.ShipperPostal = pcommFieldList.GetFieldData(12, 017);
            addon.ShipperCoCode = pcommFieldList.GetFieldData(13, 017);
            addon.ShipperCoName = data.countries.Where(x => x.co_code == addon.ShipperCoCode).FirstOrDefault().co_name;
            addon.FlowIdentifier = GetFlowIdentifier(addon.ShipperCompanyName, row);
            addon.HpCompanyName = data.clientIntel.Where(x => x.client_nr == addon.CustomerID).FirstOrDefault().company_name;
            addon.BU = pcommFieldList.GetFieldData(10, 067);
            string accNum = data.clientIntel.Where(x => x.client_nr == addon.CustomerID).FirstOrDefault().carrier_account_number;
            addon.CarrierAccountNumber = accNum;
            addon.ServiceLevel = GetServiceLevel(addon);

            return addon;
        }

        private string GetServiceLevel(Addon addon)
        {
            if (addon.ShipperCompanyName.ToLower().Contains("gebruder"))
                return "CLUSTER";
            return string.Empty;
        }

        private string GetFlowIdentifier(string shipperName, int row)
        {
            string flowIdentifier = GetExcelCellValue(mainSht.Cells[row, basicConfig.AdditionalInfoCol]);
            if (flowIdentifier == null)
            {
                if (shipperName.ToLower().Contains("schenker"))
                    return "3PLTMS";
                return "special delivery request";
            }
            return flowIdentifier;
        }

        private void FillAddonTemplate(int row, Addon addon, VDbConnection data)
        {
            mainSht.Cells[row, headers.ShipmentDate].Value = addon.ShipmentDate;
            mainSht.Cells[row, headers.Weight1].Value = addon.Weight;
            mainSht.Cells[row, headers.Weight2].Value = addon.Weight;
            mainSht.Cells[row, headers.BoxAmount].Value = addon.BoxAmount;
            mainSht.Cells[row, headers.RecipentName].Value = addon.RecipentName;
            mainSht.Cells[row, headers.CompanyName].Value = addon.HpCompanyName;
            mainSht.Cells[row, headers.RecipentAddress].Value = addon.RecipentAddress;
            mainSht.Cells[row, headers.RecipentCity].Value = addon.RecipentCity;
            mainSht.Cells[row, headers.RecipentCoName].Value = addon.RecipentCoName;
            mainSht.Cells[row, headers.RecipentPostal].Value = addon.RecipentPostal;
            mainSht.Cells[row, headers.RecipentCoCode].Value = addon.RecipentCoCode;
            mainSht.Cells[row, headers.ShipperCompanyName].Value = addon.ShipperCompanyName;
            mainSht.Cells[row, headers.ShipperAddress].Value = addon.ShipperAddress;
            mainSht.Cells[row, headers.ShipperCity].Value = addon.ShipperCity;
            mainSht.Cells[row, headers.ShipperCoName].Value = addon.ShipperCoName;
            mainSht.Cells[row, headers.ShipperPostal].Value = addon.ShipperPostal;
            mainSht.Cells[row, headers.ShipperCoCode].Value = addon.ShipperCoCode;
            mainSht.Cells[row, headers.FlowIdentifier].Value = addon.FlowIdentifier;
            mainSht.Cells[row, headers.PackID].Value = string.Join(", ", addon.Pids);
            mainSht.Cells[row, headers.BU].Value = addon.BU;
            if (!string.IsNullOrEmpty(addon.CarrierAccountNumber))
                mainSht.Cells[row, headers.CarrierAccountNumber].Value = addon.CarrierAccountNumber;

            string prefix = addon.ConsigneeID.Substring(0, 2);
            bool isEU = data.countries.Where(x => x.co_code == addon.RecipentCoCode).FirstOrDefault().isEU;
            DateTime splitDate = new DateTime(2017, 04, 01);

            if (additionalConfig.CsePrefix.Contains(prefix) && isEU && shortCompanyName == "HPI" && addon.ShipmentDate.Date >= splitDate.Date)
            {
                mainSht.Cells[row, headers.CompanyName].Value = additionalConfig.HPName;
                mainSht.Cells[row, headers.BU].Value = GetBUEU(addon.BU);
            }
            else
            {
                mainSht.Cells[row, headers.CompanyName].Value = addon.HpCompanyName;
                mainSht.Cells[row, headers.BU].Value = GetBU(addon.BU);
            }

            if (shortCompanyName == "HPE")
                mainSht.Cells[row, headers.ServiceLevel].Value = addon.ServiceLevel;
        }

        private string GetBUEU(string tempBU)
        {
            if (tempBU == "IPG")
                return "EUIPG";
            return "EUPSG";
        }

        private string GetBU(string tempBU)
        {
            if (tempBU == "IPG")
                return "IPG";
            if (tempBU == "ESG")
                return "ESS";
            if (shortCompanyName == "HPE")
                return "ESS";
            else
                return "PSG";
        }
    }
}