﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace addPriceTable
{
    class PriceTable
    {
        public PriceTable()
        {
            Rates = new List<double>();
        }

        public string PriceCode { get; set; }
        public string CO { get; set; }
        public string Currency { get; set; }
        public string EffectiveDate { get; set; }
        public string ExpiryDate { get; set; }
        public double MinimumCharge { get; set; }
        public double MaximumCharge { get; set; }
        public List<double> Rates { get; set; }
    }
}