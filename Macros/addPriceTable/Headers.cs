﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace addPriceTable
{
    class Headers
    {
        public int PriceCode { get; set; }
        public int CO { get; set; }
        public int Currency { get; set; }
        public int EffectiveDate { get; set; }
        public int ExpiryDate { get; set; }
        public int MinimumCharge { get; set; }
        public int MaximumCharge { get; set; }
        public int Rate { get; set; }
    }
}