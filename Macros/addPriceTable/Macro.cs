﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCOMMLib;
using dynamicMacroLib;

namespace addPriceTable
{
    public class Macro : DynamicMacro
    {
        private string configFileName = "addPriceTable.json";
        private Headers headers;
        private int startingRow = 2;
        private int nextRecord = 7;

        protected override string ConfigurationFileName()
        {
            return configFileName;
        }

        protected override void AdditionInitIstruction()
        {
            headers = SetConfiguration<PriceTableConfigurationInfo>().Headers;
        }
        protected override void ExcelInitCreate()
        {
            return;
        }

        protected override void MAIN()
        {
            pcommPS.SendKeys(PCOMMButtons.F6);
            pcommOIA.WaitForPCOMM();

            int row = startingRow;

            while (GetExcelCellValue(mainSht.Cells[row, basicConfig.PrimeCol]) != null)
            {
                if (GetExcelCellValue(mainSht.Cells[row, basicConfig.CheckCol]) == null)
                {
                    PriceTable pt = BuildPraceTable(row);
                    CreateCosPriceTable(pt);

                    mainSht.Cells[row, basicConfig.CheckCol].Value2 = "ok";
                }

                row += nextRecord;
            }
        }

        private PriceTable BuildPraceTable(int row)
        {
            PriceTable pt = new PriceTable();
            pt.PriceCode = GetExcelCellValue(mainSht.Cells[row, headers.PriceCode]);
            pt.CO = GetExcelCellValue(mainSht.Cells[row, headers.CO]);
            pt.Currency = GetExcelCellValue(mainSht.Cells[row, headers.Currency]);
            pt.EffectiveDate = GetExcelCellValue(mainSht.Cells[row, headers.EffectiveDate]);
            pt.ExpiryDate = GetExcelCellValue(mainSht.Cells[row, headers.ExpiryDate]);
            pt.MaximumCharge = Math.Round(double.Parse(GetExcelCellValue(mainSht.Cells[row, headers.MaximumCharge])), 2);
            pt.MinimumCharge = Math.Round(double.Parse(GetExcelCellValue(mainSht.Cells[row, headers.MinimumCharge])), 2);
            pt.Rates = GetRates(row);

            return pt;
        }

        private List<double> GetRates(int row)
        {
            List<double> rates = new List<double>();
            for (int i = row; i < row + 7; i++)
            {
                double rate = double.Parse(GetExcelCellValue(mainSht.Cells[i, headers.Rate]));
                rates.Add(rate * 100);
            }

            return rates;
        }

        private void CreateCosPriceTable(PriceTable pt)
        {
            pcommPS.SetText(pt.PriceCode, 05, 024);
            if (pt.ExpiryDate != null)
                pcommPS.SetText(pt.ExpiryDate, 06, 024);
            pcommPS.SetText(pt.CO, 04, 065);
            pcommPS.SetText(pt.EffectiveDate, 05, 065);
            pcommPS.SetText(pt.Currency, 07, 065);
            pcommPS.ClearField(08, 066);
            pcommPS.SetText(pt.MinimumCharge.ToString(), 08, 066);
            pcommPS.ClearField(09, 066);
            pcommPS.SetText(pt.MaximumCharge.ToString(), 09, 066);

            pcommPS.SendKeys(PCOMMButtons.F18);
            pcommOIA.WaitForPCOMM();

            int j = 0;
            for (int i = 12; i <= 18; i++)
            {
                pcommPS.ClearField(i, 028);
                pcommPS.SetText(pt.Rates[j].ToString(), i, 028);
                pcommPS.ClearField(i, 043);
                if (i != 18)
                    pcommPS.SetText("1", i, 052);
                else
                    pcommPS.SetText("0", i, 052);
                j++;
            }

            pcommPS.SendKeys(PCOMMButtons.ENTER);
            pcommOIA.WaitForPCOMM();
            pcommPS.SendKeys(PCOMMButtons.F10);
            pcommOIA.WaitForPCOMM();
        }
    }
}