﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCOMMLib;
using dynamicMacroLib;

namespace getProposalIntel
{
    public class Macro : DynamicMacro
    {
        private string configFileName = "getProposalIntel.json";

        private int clientNumberCol = 2;
        private int shipCol = 3;
        private int packCol = 4;
        private int kgscol = 5;
        private int wtfCol = 6;

        protected override string ConfigurationFileName()
        {
            return configFileName;
        }

        protected override void ExcelInitCreate()
        {
            return;
        }

        protected override void MAIN()
        {
            int i = 2;
            while (mainSht.Cells[i, basicConfig.PrimeCol].Value2 != null)
            {
                if (mainSht.Cells[i, basicConfig.CheckCol].Value2 == null)
                {
                    string proposal = GetExcelCellValue(mainSht.Cells[i, basicConfig.PrimeCol]);
                    string clientNumber = GetExcelCellValue(mainSht.Cells[i, clientNumberCol]);

                    pcommPS.ClearField(03, 045);
                    pcommPS.ClearField(03, 073);

                    pcommPS.SetText(proposal, 03, 045);
                    pcommPS.SetText(clientNumber, 03, 073);

                    pcommPS.SendKeys(PCOMMButtons.ENTER);
                    pcommOIA.WaitForPCOMM();

                    pcommPS.SetText("10", 11, 022);
                    pcommPS.SendKeys(PCOMMButtons.ENTER);
                    pcommOIA.WaitForPCOMM();

                    pcommFieldList.Refresh();
                    string ship = pcommFieldList.GetFieldData(21, 026);
                    string pack = pcommFieldList.GetFieldData(21, 040);
                    string kgs = pcommFieldList.GetFieldData(21, 055);
                    string wtf = pcommFieldList.GetFieldData(21, 079);

                    mainSht.Cells[i, shipCol].Value2 = ship;
                    mainSht.Cells[i, packCol].Value2 = pack;
                    mainSht.Cells[i, kgscol].Value2 = kgs;
                    mainSht.Cells[i, wtfCol].Value2 = wtf;
                    mainSht.Cells[i, basicConfig.CheckCol] = "done";

                    pcommPS.SendKeys(PCOMMButtons.F12);
                }

                i++;
            }
        }
    }
}
