﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dynamicMacroLib;
using PCOMMLib;
using System.Diagnostics;

namespace checkChargeCodesFCDN
{
    public class Macro : DynamicMacro
    {
        private string configurationFileName = "checkChargeCodesFCDN.json";

        protected override void ExcelInitCreate()
        {
            return;
        }

        protected override void MAIN()
        {
            int i = 2;

            while (mainSht.Cells[i, basicConfig.PrimeCol].Value2 != null)
            {
                if (mainSht.Cells[i, basicConfig.CheckCol].Value2 == null)
                {
                    string fcdn = (string)mainSht.Cells[i, basicConfig.PrimeCol].Value2;
                    Dictionary<string, bool> chargeCodes = GetChargeCodesFromExcel(i);

                    pcommPS.ClearField(03, 037);
                    pcommOIA.WaitForPCOMM();

                    pcommPS.SetText(fcdn, 03, 037);
                    pcommPS.SendKeys(PCOMMButtons.ENTER);
                    pcommOIA.WaitForPCOMM();

                    SearchForCode(chargeCodes, i);
                }

                i++;
            }
        }

        private void SearchForCode(Dictionary<string, bool> chargeCodes, int row)
        {
            string endOfPage;
            do
            {
                pcommFieldList.Refresh();
                endOfPage = pcommFieldList.GetFieldData(20, 074);

                for (int i = 10; i <= 19; i++)
                {
                    if (IsRecordAvailable(i))
                    {
                        string pidLatter = pcommFieldList.GetFieldData(i, 046);
                        if (pidLatter != "B")
                        {
                            pcommPS.SetText("5", i, 002);
                            pcommPS.SendKeys(PCOMMButtons.ENTER);
                            pcommOIA.WaitForPCOMM();

                            pcommPS.SendKeys(PCOMMButtons.F16);
                            pcommOIA.WaitForPCOMM();

                            pcommPS.SetText("1", 20, 015);
                            pcommPS.SendKeys(PCOMMButtons.ENTER);
                            pcommOIA.WaitForPCOMM();

                            List<string> codes = GetCodes();
                            UpdateCodesDict(chargeCodes, codes);

                            ExitFromDetails(3);
                            pcommPS.Wait(200);

                            if (IsSearchComplete(chargeCodes))
                            {
                                UpdateExcelWithOK(row);
                                return;
                            }
                        }
                    }
                    else
                    {
                        if (!IsSearchComplete(chargeCodes))
                            UpdateExcelWithError(chargeCodes, row);
                        return;
                    }
                }

                if (endOfPage != "Bottom")
                {
                    pcommPS.SendKeys(PCOMMButtons.PAGE_DOWN);
                    pcommOIA.WaitForPCOMM();
                }

            } while (endOfPage != "Bottom");

            if (!IsSearchComplete(chargeCodes))
            {
                UpdateExcelWithError(chargeCodes, row);
            }
        }

        private void UpdateExcelWithError(Dictionary<string, bool> chargeCodes, int row)
        {
            StringBuilder errorMsg = new StringBuilder();
            foreach (KeyValuePair<string, bool> item in chargeCodes)
            {
                if (!item.Value)
                    errorMsg.Append(item.Key + ", ");
            }
            errorMsg.Append("not in COS");
            mainSht.Cells[row, basicConfig.AdditionalInfoCol].Value2 = errorMsg.ToString();
            mainSht.Cells[row, basicConfig.CheckCol].Value2 = "not ok";
        }

        private void ExitFromDetails(int count)
        {
            for (int i = 1; i <= count; i++)
            {
                pcommPS.SendKeys(PCOMMButtons.F12);
                pcommOIA.WaitForPCOMM();
            }
        }

        private void UpdateExcelWithOK(int row)
        {
            mainSht.Cells[row, basicConfig.AdditionalInfoCol] = "ok";
            mainSht.Cells[row, basicConfig.CheckCol] = "ok";
        }

        private bool IsSearchComplete(Dictionary<string, bool> cc)
        {
            foreach (KeyValuePair<string, bool> code in cc)
            {
                if (!code.Value)
                    return false;
            }

            return true;
        }

        private void UpdateCodesDict(Dictionary<string, bool> cc, List<string> codes)
        {
            for (int i = 0; i < codes.Count; i++)
            {
                if (cc.ContainsKey(codes[i]))
                    cc[codes[i]] = true;
            }
        }

        private List<string> GetCodes()
        {
            List<string> codes = new List<string>();
            pcommFieldList.Refresh();

            for (int i = 9; i <= 14; i++)
            {
                string code = pcommFieldList.GetFieldData(i, 006);
                if (!string.IsNullOrWhiteSpace(code))
                {
                    if (code.Length >= 2)
                        codes.Add(code.Substring(0, 2));
                }
            }

            return codes;
        }

        private bool IsInstructionAvailable()
        {
            pcommFieldList.Refresh();
            string field = pcommFieldList.GetFieldData(13, 013);
            if (string.IsNullOrWhiteSpace(field))
                return false;
            return true;
        }

        private bool IsRecordAvailable(int i)
        {
            pcommFieldList.Refresh();
            string record = pcommFieldList.GetFieldData(i, 005);
            if (string.IsNullOrWhiteSpace(record))
                return false;
            return true;
        }

        private Dictionary<string, bool> GetChargeCodesFromExcel(int row)
        {
            Dictionary<string, bool> cc = new Dictionary<string, bool>();
            for (int col = 2; col <= 4; col++)
            {
                string code = (string)mainSht.Cells[row, col].Value2;
                if (!string.IsNullOrEmpty(code))
                    cc.Add(code, false);
            }
            return cc;
        }

        protected override string ConfigurationFileName()
        {
            return configurationFileName;
        }
    }
}
