﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace changeAddressBySTT
{
    class ShipmentInfo
    {
        public string STT { get; set; }
        public string Address { get; set; }
        public bool IsDone { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }

        public ShipmentInfo(int row, Excel.Worksheet sht)
        {
            STT = ((string)sht.Cells[row, 1].Value2).Trim();
            Address = ((string)sht.Cells[row, 3].Value2).Trim();
            CompanyName = ((string)sht.Cells[row, 2].Value2).Trim();
        }
    }
}