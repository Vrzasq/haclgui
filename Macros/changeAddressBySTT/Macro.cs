﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dynamicMacroLib;
using PCOMMLib;
using System.Diagnostics;

namespace changeAddressBySTT
{
    public class Macro : DynamicMacro
    {
        private string configFileName = "changeAddressBYSTT.json";

        protected override string ConfigurationFileName()
        {
            return configFileName;
        }

        protected override void ExcelInitCreate()
        {
            return;
        }

        protected override void MAIN()
        {
            CleanCielWindow();

            int i = 2;
            while (mainSht.Cells[i, basicConfig.PrimeCol].Value2 != null)
            {
                var lol = mainSht.Cells[i, basicConfig.PrimeCol].Value2;
                if (mainSht.Cells[i, basicConfig.CheckCol].Value2 != null)
                {
                    i++;
                    continue;
                }

                var shipmentInfo = new ShipmentInfo(i, mainSht);
                shipmentInfo.Phone = GetExcelCellValue(mainSht.Cells[i, 4]);

                pcommPS.SendKeys(PCOMMButtons.F20);
                pcommOIA.WaitForPCOMM();
                pcommPS.SetText(shipmentInfo.STT, 08, 030);
                pcommPS.SendKeys(PCOMMButtons.ENTER);
                pcommOIA.WaitForPCOMM();

                pcommFieldList.Refresh();

                var stt = pcommFieldList.GetFieldData(09, 009);
                if (stt != shipmentInfo.STT)
                {
                    ExitFromSTTWindow(i);
                    i++;
                    continue;
                }

                pcommPS.SetText("1", 09, 005);
                pcommPS.SendKeys(PCOMMButtons.ENTER);
                pcommOIA.WaitForPCOMM();

                CleanOptionFields();
                pcommOIA.WaitForPCOMM();

                pcommPS.SetText("20", 11, 002);
                pcommPS.SendKeys(PCOMMButtons.ENTER);
                pcommOIA.WaitForPCOMM();

                if (shipmentInfo.Phone != null)
                {
                    pcommPS.SendKeys(PCOMMButtons.F7);
                    pcommOIA.WaitForPCOMM();
                    pcommPS.SetText(shipmentInfo.Phone, 10, 003);
                    pcommPS.SendKeys(PCOMMButtons.ENTER);
                    pcommOIA.WaitForPCOMM();
                }

                pcommPS.SetCursorPos(08, 020);
                pcommOIA.WaitForPCOMM();

                pcommPS.SendKeys(PCOMMButtons.F10);
                pcommOIA.WaitForPCOMM();

                CleanDeliveryToInfo();
                pcommOIA.WaitForPCOMM();

                FillNewDeliveryInfo(shipmentInfo);
                pcommOIA.WaitForPCOMM();

                ConfrimChanges();

                mainSht.Cells[i, basicConfig.CheckCol].Value2 = "ok";

                i++;
            }
        }

        private void ConfrimChanges()
        {
            for (int i = 0; i <= 5; i++)
            {
                pcommPS.SendKeys(PCOMMButtons.ENTER);
                pcommOIA.WaitForPCOMM();
            }
        }

        private void FillNewDeliveryInfo(ShipmentInfo info)
        {
            pcommPS.SetText(info.CompanyName, 15, 045);
            pcommPS.SetText(info.Address, 17, 045);
        }

        private void ExitFromSTTWindow(int i)
        {
            mainSht.Cells[i, basicConfig.CheckCol].Value2 = "not found";
            pcommPS.SendKeys(PCOMMButtons.F12);
            pcommOIA.WaitForPCOMM();
        }

        private void CleanOptionFields()
        {
            pcommPS.ClearField(10, 002);
            pcommPS.ClearField(10, 006);
            pcommPS.ClearField(10, 025);
            pcommPS.ClearField(11, 002);
            pcommPS.ClearField(13, 002);
            pcommPS.ClearField(15, 002);
            pcommPS.ClearField(17, 002);
            pcommPS.ClearField(19, 002);
        }

        private void CleanCielWindow()
        {
            pcommPS.ClearField(03, 020);
            pcommPS.ClearField(04, 020);
            pcommPS.ClearField(10, 002);
            pcommPS.ClearField(10, 006);
            pcommPS.ClearField(10, 025);
        }

        private void CleanDeliveryToInfo()
        {
            pcommPS.ClearField(15, 045);
            pcommPS.ClearField(16, 045);
            pcommPS.ClearField(17, 045);
        }
    }
}