﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using dynamicMacroLib;


namespace checkChargeCodesPID
{
    public class Macro : DynamicMacro
    {
        private string configFileName = "checkChargeCodesPID.json";
        private string clientNumber;

        protected override string ConfigurationFileName()
        {
            return configFileName;
        }

        public override void INIT(string connectionName)
        {
            base.INIT(connectionName);
        }

        protected override void ExcelInitCreate()
        {
            return;
        }

        protected override void MAIN()
        {
            clientNumber = mainSht.Range[basicConfig.ClientNumberCell].Value2.ToString();
            CleanStuff();
            pcommPS.SetText(clientNumber, 03, 033);

            int i = 2;
            string pidCell = GetExcelCellValue(mainSht.Cells[i, basicConfig.PrimeCol]);
            while (!string.IsNullOrEmpty(pidCell))
            {
                string checkFiled = GetExcelCellValue(mainSht.Cells[i, basicConfig.CheckCol]);
                if (string.IsNullOrEmpty(checkFiled))
                {
                    Dictionary<string, bool> chargeCodes = GetCodesFromExcel(i);
                    string pid = GetPid(pidCell);
                    pcommPS.ClearField(03, 054);
                    pcommOIA.WaitForPCOMM();
                    pcommPS.SetText(pid, 03, 054);
                    //Thread.Sleep(100);
                    pcommPS.SendKeys(PCOMMLib.PCOMMButtons.ENTER);
                    pcommOIA.WaitForPCOMM();

                    if (!PidExists(pid))
                    {
                        i++;
                        pidCell = GetExcelCellValue(mainSht.Cells[i, basicConfig.PrimeCol]);
                        continue;
                    }

                    //.Sleep(100);
                    pcommPS.SetText("7", 10, 003);
                    pcommPS.SendKeys(PCOMMLib.PCOMMButtons.ENTER);
                    pcommOIA.WaitForPCOMM();

                    pcommPS.SetText("1", 20, 015);
                    pcommPS.SendKeys(PCOMMLib.PCOMMButtons.ENTER);
                    pcommOIA.WaitForPCOMM();
                    //Thread.Sleep(100);
                    List<string> codes = GetCodesFromPID();
                    CheckCodes(chargeCodes, codes);

                    ExitFromDetails();
                    //Thread.Sleep(100);

                    UpdateExcel(chargeCodes, i);
                }

                i++;
                pidCell = GetExcelCellValue(mainSht.Cells[i, basicConfig.PrimeCol]);
            }
        }

        private void UpdateExcel(Dictionary<string, bool> chargeCodes, int row)
        {
            if (!CodesWereFound(chargeCodes))
                UpdateExcelWithNotOK(chargeCodes, row);
            else
                UpdateExcelWithOK(row);
        }

        private void UpdateExcelWithNotOK(Dictionary<string, bool> chargeCodes, int row)
        {
            string errorMsg = BuildErrorMessage(chargeCodes);
            mainSht.Cells[row, basicConfig.AdditionalInfoCol].Value2 = errorMsg;
            mainSht.Cells[row, basicConfig.CheckCol].Value2 = "not ok";
        }

        private string BuildErrorMessage(Dictionary<string, bool> chargeCodes)
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, bool> code in chargeCodes)
            {
                if (!code.Value)
                    sb.Append(code.Key + ", ");
            }

            sb.Append("Not in COS");
            return sb.ToString();
        }

        private void UpdateExcelWithOK(int row)
        {
            mainSht.Cells[row, basicConfig.CheckCol].Value2 = "ok";
            mainSht.Cells[row, basicConfig.AdditionalInfoCol].Value2 = "ok";
        }

        private bool CodesWereFound(Dictionary<string, bool> chargeCodes)
        {
            foreach (KeyValuePair<string, bool> code in chargeCodes)
            {
                if (!code.Value)
                    return false;
            }
            return true;
        }

        private void CheckCodes(Dictionary<string, bool> chargeCodes, List<string> codes)
        {
            foreach (string code in codes)
            {
                if (chargeCodes.ContainsKey(code))
                    chargeCodes[code] = true;
            }
        }

        private void ExitFromDetails()
        {
            pcommPS.SendKeys(PCOMMLib.PCOMMButtons.F12);
            pcommOIA.WaitForPCOMM();
            pcommPS.SendKeys(PCOMMLib.PCOMMButtons.F12);
            pcommOIA.WaitForPCOMM();
        }

        private Dictionary<string, bool> GetCodesFromExcel(int row)
        {
            Dictionary<string, bool> cc = new Dictionary<string, bool>();

            for (int col = 2; col < 6; col++)
            {
                string field = GetExcelCellValue(mainSht.Cells[row, col]);
                if (!string.IsNullOrEmpty(field))
                    cc.Add(field.Trim(), false);
            }

            return cc;
        }

        private List<string> GetCodesFromPID()
        {
            List<string> codes = new List<string>();
            pcommFieldList.Refresh();

            for (int i = 9; i <= 17; i++)
            {
                string field = pcommFieldList.GetFieldData(i, 006);
                if (!string.IsNullOrWhiteSpace(field))
                {
                    if (field.Length >= 2)
                        codes.Add(field.Substring(0, 2));
                }
            }

            return codes;
        }

        private bool PidExists(string pid)
        {
            pcommFieldList.Refresh();
            string tempPid = pcommFieldList.GetFieldData(11, 016);
            return pid == tempPid;
        }

        private void CleanStuff()
        {
            pcommPS.ClearField(03, 033);
            pcommPS.ClearField(03, 054);
        }
    }
}
