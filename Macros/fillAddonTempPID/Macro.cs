﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dynamicMacroLib;
using DbAccessLayer;
using System.Globalization;
using System.IO;
using System.Configuration;

namespace fillAddonTempPID
{
    public class Macro : DynamicMacro
    {
        private string configFileName = "fillAddonTempPID.json";
        private Headers headers;
        private clientIntel ci;
        private AddonConfigurationInfo additionalConfig;
        private string shortCompanyName;

        protected override string ConfigurationFileName()
        {
            return configFileName;
        }

        protected override void ExcelInitCreate() { return; }

        protected override void AdditionInitIstruction()
        {
            additionalConfig = SetConfiguration<AddonConfigurationInfo>();
            headers = additionalConfig.Headers;
            shortCompanyName = GetExcelCellValue(mainSht.Range[basicConfig.ClientNumberCell]);
            using (VDbConnection data = new VDbConnection())
            {
                ci = data.clientIntel.Where(x => x.short_name == shortCompanyName).FirstOrDefault();
            }
        }

        protected override void MAIN()
        {
            VDbConnection data = new VDbConnection();
            pcommPS.ClearField(03, 033);
            pcommPS.SetText(ci.client_nr, 03, 033);

            int row = 25;
            string excelPID = GetExcelCellValue(mainSht.Cells[row, basicConfig.PrimeCol]);

            do
            {
                string shipDateValue = GetExcelCellValue(mainSht.Cells[row, basicConfig.CheckCol]);
                if (shipDateValue == null)
                {
                    string pid = GetPid(excelPID);
                    pcommPS.ClearField(03, 054);
                    pcommPS.SetText(pid, 03, 054);
                    pcommPS.SendKeys(PCOMMLib.PCOMMButtons.ENTER);
                    pcommOIA.WaitForPCOMM();

                    pcommPS.Wait(100);
                    pcommPS.SetText("5", 10, 003);
                    pcommPS.SendKeys(PCOMMLib.PCOMMButtons.ENTER);
                    pcommOIA.WaitForPCOMM();

                    Addon addon = GetAddonData(data, row);
                    FillAddonTemplate(row, addon, data);

                    pcommPS.SendKeys(PCOMMLib.PCOMMButtons.F12);
                    pcommOIA.WaitForPCOMM();
                }


                row++;
                excelPID = GetExcelCellValue(mainSht.Cells[row, basicConfig.PrimeCol]);
            } while (excelPID != null);

            data.Dispose();
        }

        private Addon GetAddonData(VDbConnection data, int row)
        {
            Addon addon = new Addon();
            pcommFieldList.Refresh();

            addon.PackID = pcommFieldList.GetFieldData(04, 020);
            addon.ConsigneeID = pcommFieldList.GetFieldData(06, 017);
            addon.CustomerID = pcommFieldList.GetFieldData(03, 020);
            addon.BoxAmount = int.Parse(pcommFieldList.GetFieldData(16, 023));
            addon.Weight = double.Parse(pcommFieldList.GetFieldData(16, 064));
            addon.ShipmentDate = DateTime.ParseExact((pcommFieldList.GetFieldData(06, 073)), "yyyy-MM-dd", CultureInfo.InvariantCulture);
            addon.RecipentName = pcommFieldList.GetFieldData(07, 017);
            addon.RecipentAddress = pcommFieldList.GetFieldData(09, 017) + " " + pcommFieldList.GetFieldData(10, 017);
            addon.RecipentCity = pcommFieldList.GetFieldData(11, 017);
            addon.RecipentPostal = pcommFieldList.GetFieldData(12, 017);
            addon.RecipentCoCode = pcommFieldList.GetFieldData(13, 017);
            addon.RecipentCoName = data.countries.Where(x => x.co_code == addon.RecipentCoCode).FirstOrDefault().co_name;
            addon.BU = pcommFieldList.GetFieldData(10, 067);

            pcommPS.SendKeys(PCOMMLib.PCOMMButtons.PAGE_DOWN);
            pcommOIA.WaitForPCOMM();

            pcommFieldList.Refresh();

            addon.ShipperCompanyName = pcommFieldList.GetFieldData(07, 017);
            addon.ShipperAddress = pcommFieldList.GetFieldData(09, 017);
            addon.ShipperCity = pcommFieldList.GetFieldData(11, 017);
            addon.ShipperPostal = pcommFieldList.GetFieldData(12, 017);
            addon.ShipperCoCode = pcommFieldList.GetFieldData(13, 017);
            addon.ShipperCoName = data.countries.Where(x => x.co_code == addon.ShipperCoCode).FirstOrDefault().co_name;
            addon.FlowIdentifier = GetFlowIdentifier(addon.ShipperCompanyName, row);
            addon.HpCompanyName = ci.company_name;
            addon.ServiceLevel = GetServiceLevel(addon);

            return addon;
        }

        private string GetServiceLevel(Addon addon)
        {
            if (addon.ShipperCompanyName.ToLower().Contains("gebruder"))
                return "CLUS";
            return string.Empty;
        }

        private string GetFlowIdentifier(string shipperName, int row)
        {
            string flowIdentifier = GetExcelCellValue(mainSht.Cells[row, basicConfig.AdditionalInfoCol]);
            if (flowIdentifier == null)
            {
                if (shipperName.ToLower().Contains("schenker"))
                    return "3PLTMS";
                return "special delivery request";
            }
            return flowIdentifier;
        }

        private void FillAddonTemplate(int row, Addon addon, VDbConnection data)
        {
            mainSht.Cells[row, headers.ShipmentDate].Value = addon.ShipmentDate;
            mainSht.Cells[row, headers.Weight1].Value = addon.Weight;
            mainSht.Cells[row, headers.Weight2].Value = addon.Weight;
            mainSht.Cells[row, headers.BoxAmount].Value = addon.BoxAmount;
            mainSht.Cells[row, headers.RecipentName].Value = addon.RecipentName;
            mainSht.Cells[row, headers.RecipentAddress].Value = addon.RecipentAddress;
            mainSht.Cells[row, headers.RecipentCity].Value = addon.RecipentCity;
            mainSht.Cells[row, headers.RecipentCoName].Value = addon.RecipentCoName;
            mainSht.Cells[row, headers.RecipentPostal].Value = addon.RecipentPostal;
            mainSht.Cells[row, headers.RecipentCoCode].Value = addon.RecipentCoCode;
            mainSht.Cells[row, headers.ShipperCompanyName].Value = addon.ShipperCompanyName;
            mainSht.Cells[row, headers.ShipperAddress].Value = addon.ShipperAddress;
            mainSht.Cells[row, headers.ShipperCity].Value = addon.ShipperCity;
            mainSht.Cells[row, headers.ShipperCoName].Value = addon.ShipperCoName;
            mainSht.Cells[row, headers.ShipperPostal].Value = addon.ShipperPostal;
            mainSht.Cells[row, headers.ShipperCoCode].Value = addon.ShipperCoCode;
            mainSht.Cells[row, headers.FlowIdentifier].Value = addon.FlowIdentifier;
            mainSht.Cells[row, headers.PackID].Value = addon.PackID;
            if (!string.IsNullOrEmpty(ci.carrier_account_number))
                mainSht.Cells[row, headers.CarrierAccountNumber].Value = ci.carrier_account_number;

            string prefix = addon.ConsigneeID.Substring(0, 2);
            bool isEU = data.countries.Where(x => x.co_code == addon.RecipentCoCode).FirstOrDefault().isEU;
            DateTime splitDate = new DateTime(2017, 04, 01);

            if (additionalConfig.CsePrefix.Contains(prefix) && isEU && shortCompanyName == "HPI" && addon.ShipmentDate.Date >= splitDate.Date)
            {
                mainSht.Cells[row, headers.CompanyName].Value = additionalConfig.HPName;
                mainSht.Cells[row, headers.BU].Value = GetBUEU(addon.BU);
            }
            else
            {
                mainSht.Cells[row, headers.CompanyName].Value = addon.HpCompanyName;
                mainSht.Cells[row, headers.BU].Value = GetBU(addon.BU);
            }

            if (shortCompanyName == "HPE")
                mainSht.Cells[row, headers.ServiceLevel].Value = addon.ServiceLevel;

        }


        private string GetBUEU(string tempBU)
        {
            if (tempBU == "IPG")
                return "EUIPG";
            return "EUPSG";
        }

        private string GetBU(string tempBU)
        {
            if (tempBU == "IPG")
                return "IPG";
            if (tempBU == "ESG")
                return "ESS";
            if (shortCompanyName == "HPE")
                return "ESS";
            else
                return "PSG";
        }
    }
}