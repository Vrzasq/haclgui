﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fillAddonTempPID
{
    public class Headers
    {
        public int ShipmentDate { get; set; }
        public int Weight1 { get; set; }
        public int Weight2 { get; set; }
        public int BoxAmount { get; set; }
        public int RecipentName { get; set; }
        public int CompanyName { get; set; }
        public int RecipentAddress { get; set; }
        public int RecipentCity { get; set; }
        public int RecipentCoName { get; set; }
        public int RecipentPostal { get; set; }
        public int RecipentCoCode { get; set; }
        public int ShipperCompanyName { get; set; }
        public int ShipperAddress { get; set; }
        public int ShipperCity { get; set; }
        public int ShipperCoName { get; set; }
        public int ShipperPostal { get; set; }
        public int ShipperCoCode { get; set; }
        public int FlowIdentifier { get; set; }
        public int PackID { get; set; }
        public int BU { get; set; }
        public int FCDN { get; set; }
        public int CarrierAccountNumber { get; set; }
        public int ServiceLevel { get; set; }
    }
}