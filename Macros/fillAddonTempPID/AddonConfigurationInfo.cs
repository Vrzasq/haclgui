﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dynamicMacroLib;

namespace fillAddonTempPID
{
    public class AddonConfigurationInfo : ConfigurationInfo
    {
        public Headers Headers { get; set; }
        public HashSet<string> CsePrefix { get; set; }
        public string HPName { get; set; }
    }
}
