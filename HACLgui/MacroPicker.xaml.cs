﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;

namespace HACLgui
{
    /// <summary>
    /// Interaction logic for MacroPicker.xaml
    /// </summary>
    public partial class MacroPicker : Window
    {
        private List<Assembly> assemblies = new List<Assembly>();
        private List<string> macroNames = new List<string>();

        public MacroPicker()
        {
            InitializeComponent();
        }

        private void button_info_Click(object sender, RoutedEventArgs e)
        {
            if (listBox.SelectedIndex != -1)
                MessageBox.Show(assemblies[listBox.SelectedIndex].GetCustomAttribute<AssemblyDescriptionAttribute>().Description);
        }

        private void button_select_Click(object sender, RoutedEventArgs e)
        {
            if (assemblies.Count > 0 && listBox.SelectedIndex != -1)
            {
                Tag = assemblies[listBox.SelectedIndex];
                DialogResult = true;
            }
        }

        private void button_cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void MackroPicker_Loaded(object sender, RoutedEventArgs e)
        {
            BuildAssemblyList();
            GetMacrosNames();
            listBox.ItemsSource = macroNames;
        }

        private void BuildAssemblyList()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "macros_dll";
            string[] files = System.IO.Directory.GetFiles(path, "*.dll");

            for (int i = 0; i < files.Length; i++)
                assemblies.Add(Assembly.LoadFile(files[i]));
        }

        private void GetMacrosNames()
        {
            foreach (var lib in assemblies)
                macroNames.Add(lib.GetCustomAttribute<AssemblyTitleAttribute>().Title);
        }
    }
}
