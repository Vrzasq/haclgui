﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;
using System.Reflection;
using dynamicMacroLib;
using PCOMMLib;
using Newtonsoft.Json;

namespace HACLgui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer timer = new DispatcherTimer();
        private Dictionary<string, BackgroundWorker> workers = new Dictionary<string, BackgroundWorker>
        {
            {"A", new BackgroundWorker() },
            {"B", new BackgroundWorker() },
            {"C", new BackgroundWorker() },
            {"D", new BackgroundWorker() },
            {"E", new BackgroundWorker() },
            {"F", new BackgroundWorker() }
        };
        PCOMMConnList conList = new PCOMMConnList();

        private DynamicMacro macro;

        public MainWindow()
        {
            InitializeComponent();
            timer.Interval = TimeSpan.FromSeconds(3);
            timer.Tick += Timer_Tick;
            AttachDoWorkEvent();
            timer.Start();
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory;
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
            string connectionName = (string)e.Argument;
            macro.INIT(connectionName);
            macro.Run();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            CheckConnectionStatus();
        }

        private void button_RunMacro_Click(object sender, RoutedEventArgs e)
        {
            var mp = new MacroPicker();
            string connectionName = SetConnectionName();

            if (mp.ShowDialog() == true && connectionName != string.Empty)
            {
                Assembly asm = (Assembly)mp.Tag;
                var fullTypeName = asm.GetName().Name + ".Macro";
                macro = (DynamicMacro)asm.CreateInstance(fullTypeName);
                macro.Complete += Macro_Complete;
                macro.Error += Macro_Error;

                conList.FindConnectionByName(connectionName).ChangeState(PCOMMConnectionState.BUSY);
                workers[connectionName].RunWorkerAsync(connectionName);
            }
        }

        private void Macro_Error(object sender, MacroEventArgs e)
        {
            if (e.UnSub)
                UnsubFromEvents((DynamicMacro)sender);
            conList.FindConnectionByName(e.SessionName).ChangeState(PCOMMConnectionState.ACTIVE);
            if (e.AdditionalData != null)
                MessageBox.Show(e.AdditionalInfo + Environment.NewLine + JsonConvert.SerializeObject(e.AdditionalData, Formatting.Indented));
            else
                MessageBox.Show(e.AdditionalInfo);
        }

        private void Macro_Complete(object sender, MacroEventArgs e)
        {
            UnsubFromEvents((DynamicMacro)sender);
            conList.FindConnectionByName(e.SessionName).ChangeState(PCOMMConnectionState.ACTIVE);
            MessageBox.Show(e.AdditionalInfo);
        }

        private string SetConnectionName()
        {
            if (comboBox.SelectedItem != null)
                return (string)comboBox.SelectedItem;
            return string.Empty;
        }

        private void CheckConnectionStatus()
        {
            conList.Refresh();
            var connectionIndactors = canvas_ConnectionsStatus.Children.OfType<Canvas>().SelectMany(x => x.Children.OfType<Ellipse>());
            foreach (var indactor in connectionIndactors)
            {
                var connection = conList.FindConnectionByName((string)indactor.Tag);
                SetConnectionIndicatorColor(connection, indactor);
            }

            var conNames = conList.GetActiveConnections().Select(x => x.Name);
            comboBox.ItemsSource = conNames;

            TogleRunMacroButton(conNames);
        }

        private void TogleRunMacroButton(IEnumerable<string> conNames)
        {
            if (conNames.Count() > 0)
                button_RunMacro.IsEnabled = true;
            else
                button_RunMacro.IsEnabled = false;
        }


        private void SetConnectionIndicatorColor(PCOMMConn connection, Ellipse control)
        {
            switch (connection.State)
            {
                case PCOMMConnectionState.INACTIVE:
                    control.Fill = Brushes.Red;
                    break;
                case PCOMMConnectionState.ACTIVE:
                    control.Fill = Brushes.Green;
                    break;
                case PCOMMConnectionState.BUSY:
                    control.Fill = Brushes.Yellow;
                    break;
                default:
                    break;
            }
        }

        private void UnsubFromEvents(DynamicMacro dm)
        {
            dm.Complete -= Macro_Complete;
            dm.Error -= Macro_Error;
        }

        private void AttachDoWorkEvent()
        {
            foreach (KeyValuePair<string, BackgroundWorker> item in workers)
            {
                item.Value.DoWork += Worker_DoWork;
            }
        }
    }
}
