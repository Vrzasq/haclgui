﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbAccessLayer
{
    public class VDbConnection : DbContext
    {
        public VDbConnection() : base("dataConnection") { }
        public VDbConnection(string connectionString) : base(connectionString) { }

        public virtual DbSet<countries> countries { get; set; }
        public virtual DbSet<vatCodes> vatCodes { get; set; }
        public virtual DbSet<clientIntel> clientIntel { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();
        }
    }
}
