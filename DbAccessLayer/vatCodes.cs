﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbAccessLayer
{
    public class vatCodes
    {
        [Key]
        public int sn { get; set; }
        public int client_id { get; set; }
        public string vat_code { get; set; }
        public string lane_type { get; set; }

        public virtual clientIntel clientIntel { get; set; }
    }
}
