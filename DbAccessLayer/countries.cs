﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbAccessLayer
{
    public class countries
    {
        [Key]
        public int sn { get; set; }
        public string co_code { get; set; }
        public string co_name { get; set; }
        public bool isEU { get; set; }
    }
}
