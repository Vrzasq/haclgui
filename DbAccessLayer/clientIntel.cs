﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbAccessLayer
{
    public class clientIntel
    {
        public clientIntel()
        {
            vatCodes = new HashSet<vatCodes>();
        }

        [Key]
        public int client_id { get; set; }
        public string short_name { get; set; }
        public string client_nr { get; set; }
        public string company_name { get; set; }
        public string carrier_account_number { get; set; }

        public virtual ICollection<vatCodes> vatCodes { get; set; }
    }
}
